import { controls } from '../../constants/controls';
import { playMusic } from '../components/modal/modal';
class Player {
  constructor(fighter) {
    this.name = fighter.name;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.initialHealth = fighter.health;
    this.currentHealth = fighter.health;
    this.blocking = false;
    this.criticalHitSequence = [];
    this.criticalHitTiming = 0;
    this.lastCriticalHit = 0;
  }
}

export async function fight(firstFighter, secondFighter) {
  playMusic('../../../../resources/main-song.mp3', 0.01);
  return new Promise((resolve) => {
    const firstPlayer = new Player(firstFighter);
    const secondPlayer = new Player(secondFighter);

    const keyDownListener = (event) => {
      checkKeyPress(event.code, firstPlayer, secondPlayer);

      if (firstPlayer.currentHealth <= 0) {
        document.removeEventListener('keydown', keyDownListener);
        document.removeEventListener('keyup', keyUpListener);
        resolve(secondFighter);
      } else if (secondPlayer.currentHealth <= 0) {
        document.removeEventListener('keydown', keyDownListener);
        document.removeEventListener('keyup', keyUpListener);
        resolve(firstFighter);
      }
    };
    document.addEventListener('keydown', keyDownListener);

    const keyUpListener = (event) => {
      if (event.code === controls.PlayerOneBlock && firstPlayer.blocking) {
        firstPlayer.blocking = false;
      } else if (event.code === controls.PlayerTwoBlock && secondPlayer.blocking) {
        secondPlayer.blocking = false;
      } else {
        console.log('SLAVA UKRAINI');
      }
    };
    document.addEventListener('keyup', keyUpListener);
  });
}

export function getCriticalHit(fighter) {
  // return critical damage
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  // return damage
  if (attacker.blocking) {
    return 0;
  }

  if (!defender.blocking) {
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
  }
  return 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const { attack } = fighter;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const { defense } = fighter;
  return defense * dodgeChance;
}

const doDefaultCriticalParametrs = (fighter) => {
  fighter.criticalHitSequence = [];
  fighter.criticalHitTiming = 0;
};

const checkKeyPress = (keyCode, firstFighter, secondFighter) => {
  const playerOneHelthbar = document.getElementById('left-fighter-indicator');
  const playerTwoHelthbar = document.getElementById('right-fighter-indicator');

  const playerLeftImg = document.querySelector('.arena___left-fighter img');
  const playerRightImg = document.querySelector('.arena___right-fighter img');
  playerLeftImg.style.margin = '0';
  playerRightImg.style.margin = '0';
  console.log(playerLeftImg);
  switch (keyCode) {
    case controls.PlayerOneAttack:
      // handlePlayerAttack
      playerTwoHelthbar.style.background = 'red';
      playerLeftImg.style.marginLeft = 'auto';
      setTimeout(() => {
        playerTwoHelthbar.style.background = '#ebd759';
        playerLeftImg.style.marginLeft = '0';
      }, 100);
      const secondPlayerDamageDealt = getDamage(firstFighter, secondFighter);
      secondFighter.currentHealth -= secondPlayerDamageDealt;
      doDefaultCriticalParametrs(firstFighter);
      updateHealthIndicator(secondFighter, 'right');

      break;
    case controls.PlayerTwoAttack:
      // handlePlayerAttack
      playerOneHelthbar.style.background = 'red';
      playerRightImg.style.marginRight = 'auto';
      setTimeout(() => {
        playerOneHelthbar.style.background = '#ebd759';
        playerRightImg.style.marginRight = '0';
      }, 100);
      const firstPlayerDealt = getDamage(secondFighter, firstFighter);
      firstFighter.currentHealth -= firstPlayerDealt;
      doDefaultCriticalParametrs(secondFighter);
      updateHealthIndicator(firstFighter, 'left');
      break;
    case controls.PlayerOneBlock:
      if (!firstFighter.blocking) {
        firstFighter.blocking = true;
      }
      break;
    case controls.PlayerTwoBlock:
      if (!secondFighter.blocking) {
        secondFighter.blocking = true;
      }
      break;
    default:
      if (controls.PlayerOneCriticalHitCombination.includes(keyCode) && !firstFighter.blocking) {
        if (
          checkCriticalHitSequence(
            firstFighter,
            secondFighter,
            keyCode,
            controls.PlayerOneCriticalHitCombination,
            'right'
          )
        ) {
          playerTwoHelthbar.style.background = '#a31f1f';
          playerLeftImg.style.marginLeft = 'auto';
          setTimeout(() => {
            playerTwoHelthbar.style.background = '#ebd759';
            playerLeftImg.style.marginLeft = '0';
          }, 500);
        }
      } else if (controls.PlayerTwoCriticalHitCombination.includes(keyCode) && !secondFighter.blocking) {
        if (
          checkCriticalHitSequence(
            secondFighter,
            firstFighter,
            keyCode,
            controls.PlayerTwoCriticalHitCombination,
            'left'
          )
        ) {
          playerOneHelthbar.style.background = '#a31f1f';
          playerRightImg.style.marginRight = 'auto';
          setTimeout(() => {
            playerOneHelthbar.style.background = '#ebd759';
            playerRightImg.style.marginRight = '0';
          }, 500);
        }
      } else {
        console.log('SLAVA UKRAINI');
      }
  }
};

const updateHealthIndicator = (fighter, side) => {
  document.getElementById(`${side}-fighter-indicator`).style.width = `${Math.round(
    (fighter.currentHealth / fighter.initialHealth) * 100
  )}%`;
};

const checkCriticalHitSequence = (attacker, defender, key, combination, side) => {
  if (attacker.criticalHitSequence.length === 0) {
    if ((new Date() - attacker.lastCriticalHit) / 1000 < 10) {
      console.log('Critical hit every 10 sec');
    } else if (key === combination[0]) {
      attacker.criticalHitTiming = new Date();
      attacker.criticalHitSequence.push(key);
    }
  } else if (attacker.criticalHitSequence.length === 1) {
    if ((new Date() - attacker.criticalHitTiming) / 1000 > 2) {
      doDefaultCriticalParametrs(attacker);
    } else if (key === combination[1]) {
      attacker.criticalHitTiming = new Date();
      attacker.criticalHitSequence.push(key);
    } else {
      doDefaultCriticalParametrs(attacker);
    }
  } else if (attacker.criticalHitSequence.length === 2) {
    if ((new Date() - attacker.criticalHitTiming) / 1000 > 2) {
      doDefaultCriticalParametrs(attacker);
    } else if (key === combination[2]) {
      const defenderPlayerDamageDealt = getCriticalHit(attacker);
      defender.currentHealth -= defenderPlayerDamageDealt;

      updateHealthIndicator(defender, side);
      doDefaultCriticalParametrs(attacker);

      attacker.lastCriticalHit = new Date();
      return 1;
    } else {
      doDefaultCriticalParametrs(attacker);
    }
  } else {
    return 0;
  }
  return 0;
};
