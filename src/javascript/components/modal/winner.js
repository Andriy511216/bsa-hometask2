import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({
    title: `Fight winner - ${fighter.name}`,
    bodyElement: fighter,
    onClose: () => {
      location.reload();
    },
  });
}
